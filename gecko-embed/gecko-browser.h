/*
 *  Copyright (C) 2000-2004 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#ifndef GECKO_BROWSER_H
#define GECKO_BROWSER_H

#include <gtkmozembed.h>

#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

#define GECKO_TYPE_BROWSER		(gecko_browser_get_type ())
#define GECKO_BROWSER(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), GECKO_TYPE_BROWSER, GeckoBrowser))
#define GECKO_BROWSER_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), GECKO_TYPE_BROWSER, GeckoBrowserClass))
#define GECKO_IS_EMBED(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), GECKO_TYPE_BROWSER))
#define GECKO_IS_EMBED_CLASS(k)		(G_TYPE_CHECK_CLASS_TYPE ((k), GECKO_TYPE_BROWSER))
#define GECKO_BROWSER_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), GECKO_TYPE_BROWSER, GeckoBrowserClass))

typedef struct GeckoBrowserClass	GeckoBrowserClass;
typedef struct GeckoBrowser		GeckoBrowser;
typedef struct GeckoBrowserPrivate	GeckoBrowserPrivate;

typedef struct GeckoBrowserBase 	GeckoBrowserBase;

/* TODO
typedef struct {
} GeckoBrowserBase;
*/

struct GeckoBrowser
{
        GtkMozEmbed parent;

	/*< private >*/
        GeckoBrowserPrivate *priv;
};

struct GeckoBrowserClass
{
        GtkMozEmbedClass parent_class;

	gint (* open_address) (GeckoBrowser *self, char *address);

	GeckoBrowserBase * (* create_impl) (GeckoBrowser *self);
	// TODO void * (* create_impl) (GeckoBrowser *self);
};

void	      gecko_browser_set_profile_path	(const char *path);

GType	      gecko_browser_get_type		(void);
GeckoBrowser *gecko_browser_new			(void);
void	      gecko_browser_load_address	(GeckoBrowser *self,
						 const char   *address);
gboolean      gecko_browser_can_go_back		(GeckoBrowser *self);
gboolean      gecko_browser_can_go_forward	(GeckoBrowser *self);
void          gecko_browser_go_back		(GeckoBrowser *self);
void          gecko_browser_go_forward		(GeckoBrowser *self);
void	      gecko_browser_reload		(GeckoBrowser *self);
char         *gecko_browser_get_title		(GeckoBrowser *self);
char	     *gecko_browser_get_address		(GeckoBrowser *self);
GList        *gecko_browser_list_images		(GeckoBrowser *self);

G_END_DECLS

#endif /* !GECKO_BROWSER_H */
