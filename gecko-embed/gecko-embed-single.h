/*
 *  Copyright (C) 2000-2003 Marco Pesenti Gritti
 *  Copyright (C) 2003 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#ifndef GECKO_EMBED_SINGLE_H
#define GECKO_EMBED_SINGLE_H

#include <glib-object.h>

G_BEGIN_DECLS

#define GECKO_TYPE_EMBED_SINGLE		(gecko_embed_single_get_type ())
#define GECKO_EMBED_SINGLE(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), GECKO_TYPE_EMBED_SINGLE, GeckoEmbedSingle))
#define GECKO_EMBED_SINGLE_CLASS(k)	(G_TYPE_CHECK_CLASS_CAST((k), GECKO_TYPE_EMBED_SINGLE, GeckoEmbedSingleClass))
#define GECKO_IS_EMBED_SINGLE(o)	(G_TYPE_CHECK_INSTANCE_TYPE ((o), GECKO_TYPE_EMBED_SINGLE))
#define GECKO_IS_EMBED_SINGLE_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), GECKO_TYPE_EMBED_SINGLE))
#define GECKO_EMBED_SINGLE_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), GECKO_TYPE_EMBED_SINGLE, GeckoEmbedSingleClass))

typedef struct GeckoEmbedSingle		GeckoEmbedSingle;
typedef struct GeckoEmbedSingleClass	GeckoEmbedSingleClass;
typedef struct GeckoEmbedSinglePrivate	GeckoEmbedSinglePrivate;

struct GeckoEmbedSingle
{
	GObject parent;

	/*< private >*/
	GeckoEmbedSinglePrivate *priv;
};

struct GeckoEmbedSingleClass
{
	GObjectClass parent_class;
};

GType	          gecko_embed_single_get_type	      (void);
GeckoEmbedSingle *gecko_embed_single_new              (void);
void		  gecko_embed_single_set_profile_path (GeckoEmbedSingle *single,
						       const char       *path);
gboolean	  gecko_embed_single_startup          (GeckoEmbedSingle *single);

G_END_DECLS

#endif
