/*
 *  Copyright (C) 2000-2004 Marco Pesenti Gritti
 *  Copyright (C) 2003, 2004 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#include "mozilla-config.h"

#include "config.h"

#include "gecko-embed/gecko-browser.h"
#include "gecko-embed/gecko-embed-single.h"
#include "gecko-embed/gecko-embed-marshal.h"

#include "Browser.h"

#include <gtkmozembed.h>
#include <nsIURI.h>
#include <nsEmbedString.h>

#define GECKO_BROWSER_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GECKO_TYPE_BROWSER, GeckoBrowserPrivate))

struct GeckoBrowserPrivate
{
	Browser *browser;
};

enum
{
	OPEN_ADDRESS,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

static GeckoEmbedSingle *gecko_embed_single = NULL;
static gboolean single_startup = FALSE;

static GObjectClass *parent_class = NULL;

static void
gecko_browser_realize (GtkWidget *widget)
{
	GeckoBrowserPrivate *mpriv = GECKO_BROWSER (widget)->priv;

	GTK_WIDGET_CLASS (parent_class)->realize (widget);

	/* Initialise our helper class */
	nsresult rv;
	rv = mpriv->browser->Init (GTK_MOZ_EMBED (widget));
	if (NS_FAILED (rv))
	{
		g_warning ("EphyBrowser initialization failed for %p\n", widget);
		return;
	}
}

static void
gecko_browser_destroy (GtkObject *object)
{
	GeckoBrowser *self = GECKO_BROWSER (object);

	if (self->priv->browser)
	{
		self->priv->browser->Destroy();
	}
	
	GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void
ensure_single(void)
{
	if (gecko_embed_single == NULL)
	{
		gecko_embed_single = gecko_embed_single_new();
	}
}

static GObject *
gecko_browser_constructor (GType type,
			 guint n_construct_properties,
			 GObjectConstructParam *construct_params)
{
	ensure_single();
	if (!single_startup) {
		gecko_embed_single_startup(gecko_embed_single);
		single_startup = TRUE;
	}

	return parent_class->constructor (type, n_construct_properties,
					  construct_params);
}

static GeckoBrowserBase *
gecko_browser_create_impl (GeckoBrowser *self)
{
	GeckoBrowserPrivate *mpriv = GECKO_BROWSER_GET_PRIVATE (self);

	mpriv->browser = new Browser ();
	return mpriv->browser;
}

static void
gecko_browser_finalize (GObject *object)
{
	GeckoBrowser *self = GECKO_BROWSER (object);

	if (self->priv->browser)
	{
        	delete self->priv->browser;
	       	self->priv->browser = nsnull;
	}

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gecko_browser_class_init (GeckoBrowserClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);
     	GtkObjectClass *gtk_object_class = GTK_OBJECT_CLASS (klass); 
     	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass); 

	parent_class = (GObjectClass *) g_type_class_peek_parent (klass);

	object_class->constructor = gecko_browser_constructor;
	object_class->finalize = gecko_browser_finalize;

	gtk_object_class->destroy = gecko_browser_destroy;

	widget_class->realize = gecko_browser_realize;

	klass->create_impl = gecko_browser_create_impl;

	signals[OPEN_ADDRESS] =
		g_signal_new ("open-address",
			      G_OBJECT_CLASS_TYPE (object_class),
			      (GSignalFlags)(G_SIGNAL_RUN_FIRST | G_SIGNAL_RUN_LAST),
			      G_STRUCT_OFFSET (GeckoBrowserClass, open_address),
			      NULL, NULL,
			      gecko_embed_marshal_INT__STRING,
			      G_TYPE_INT,
			      1,
			      G_TYPE_STRING);

	g_type_class_add_private (object_class, sizeof(GeckoBrowserPrivate));
}

static gint
gecko_browser_load_uri (GeckoBrowser 	*self, 
			char 		*uri, 
			gpointer 	 data)
{
	gint result;

	g_signal_emit (self, signals[OPEN_ADDRESS], 0, uri, &result);

	return result;
}

static void
gecko_browser_init (GeckoBrowser *self)
{
	GeckoBrowserBase *base;

        self->priv = GECKO_BROWSER_GET_PRIVATE (self);

	base = GECKO_BROWSER_GET_CLASS (self)->create_impl (self);
	/* my c++ fu is letting me down on that -Rob
	self->priv->browser = dynamic_cast<Browser *> (base); */
	self->priv->browser = reinterpret_cast<Browser *> (base);

	g_signal_connect (self, "open-uri", (GCallback)gecko_browser_load_uri, NULL);
}

G_DEFINE_TYPE (GeckoBrowser, gecko_browser, GTK_TYPE_MOZ_EMBED)

GeckoBrowser *
gecko_browser_new (void)
{
	return GECKO_BROWSER (g_object_new (GECKO_TYPE_BROWSER, NULL));
}

void
gecko_browser_load_address (GeckoBrowser 	*self, 
			    const char 		*address)
{
        gtk_moz_embed_load_url (GTK_MOZ_EMBED(self), address);
}

gboolean
gecko_browser_can_go_back (GeckoBrowser *self)
{
	return gtk_moz_embed_can_go_back (GTK_MOZ_EMBED(self));
}

gboolean
gecko_browser_can_go_forward (GeckoBrowser *self)
{
	return gtk_moz_embed_can_go_forward (GTK_MOZ_EMBED(self));
}

void
gecko_browser_go_back (GeckoBrowser *self)
{
	gtk_moz_embed_go_back (GTK_MOZ_EMBED(self));
}

void
gecko_browser_go_forward (GeckoBrowser *self)
{
	gtk_moz_embed_go_forward (GTK_MOZ_EMBED(self));
}

void
gecko_browser_reload (GeckoBrowser *self)
{
	gtk_moz_embed_reload (GTK_MOZ_EMBED(self), 0);
}

char *
gecko_browser_get_title (GeckoBrowser *self)
{
	return gtk_moz_embed_get_title (GTK_MOZ_EMBED(self));
}

char *
gecko_browser_get_address (GeckoBrowser *self)
{
	GeckoBrowserPrivate *mpriv = GECKO_BROWSER(self)->priv;
	nsresult rv;

	nsCOMPtr<nsIURI> uri;
	rv = mpriv->browser->GetDocumentURI (getter_AddRefs (uri));
	if (NS_FAILED (rv)) return NULL;

	nsCOMPtr<nsIURI> furi;
	rv = uri->Clone (getter_AddRefs (furi));
	/* Some nsIURI impls return NS_OK even though they didn't put anything in the outparam!! */
	if (NS_FAILED (rv) || !furi) furi.swap(uri);

	nsEmbedCString url;
	furi->GetSpec (url);

	return url.Length() ? g_strdup (url.get()) : NULL;
}

GList *
gecko_browser_list_images (GeckoBrowser *self)
{
	GList *images = NULL;
	GeckoBrowserPrivate *mpriv = GECKO_BROWSER(self)->priv;

	nsresult rv;

	PRUint32 index;
	rv = mpriv->browser->GetNImages(&index);
	if (NS_FAILED(rv)) return NULL;

	for (PRUint32 i = 0; i < index; i++)
	{
		nsEmbedString address;
		mpriv->browser->GetImageAddress(i, address);

		nsEmbedCString cAddress;
                NS_UTF16ToCString (address, NS_CSTRING_ENCODING_UTF8, cAddress);

                images = g_list_append (images, g_strdup (cAddress.get()));
	}

	return images;
}

void
gecko_browser_set_profile_path (const char *path)
{
	ensure_single();
	gecko_embed_single_set_profile_path(gecko_embed_single, path);
}
