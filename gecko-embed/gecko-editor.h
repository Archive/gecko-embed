/*
 *  Copyright (C) 2006 Rob Staudinger
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GECKO_EDITOR_H
#define GECKO_EDITOR_H

#include <gecko-embed/gecko-browser.h>

#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

#define GECKO_TYPE_EDITOR		(gecko_editor_get_type ())
#define GECKO_EDITOR(o)			(G_TYPE_CHECK_INSTANCE_CAST ((o), GECKO_TYPE_EDITOR, GeckoEditor))
#define GECKO_EDITOR_CLASS(k)		(G_TYPE_CHECK_CLASS_CAST((k), GECKO_TYPE_EDITOR, GeckoEditorClass))
#define GECKO_IS_EDITOR(o)		(G_TYPE_CHECK_INSTANCE_TYPE ((o), GECKO_TYPE_EDITOR))
#define GECKO_IS_EDITOR_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k), GECKO_TYPE_EDITOR))
#define GECKO_EDITOR_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o), GECKO_TYPE_EDITOR, GeckoEditorClass))

typedef struct GeckoEditorClass		GeckoEditorClass;
typedef struct GeckoEditor		GeckoEditor;
typedef struct GeckoEditorPrivate	GeckoEditorPrivate;

struct GeckoEditor
{
        GeckoBrowser parent;

	/*< private >*/
        GeckoEditorPrivate *priv;
};

struct GeckoEditorClass
{
        GeckoBrowserClass parent_class;
};

GType	     gecko_editor_get_type	(void);
GeckoEditor *gecko_editor_new		(void);

G_END_DECLS

#endif /* GECKO_EDITOR_H */
