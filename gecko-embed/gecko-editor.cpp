/*
 *  Copyright (C) 2006 Rob Staudinger
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "mozilla-config.h"

#include "config.h"

#include "gecko-editor.h"

#include "Editor.h"

#define GECKO_EDITOR_GET_PRIVATE(object)(G_TYPE_INSTANCE_GET_PRIVATE ((object), GECKO_TYPE_EDITOR, GeckoEditorPrivate))

struct GeckoEditorPrivate
{
	Editor *editor;
};

static GObjectClass *parent_class = NULL;

static GeckoBrowserBase *
gecko_editor_create_impl (GeckoBrowser *self)
{
	GeckoEditorPrivate *mpriv = GECKO_EDITOR_GET_PRIVATE (self);

	mpriv->editor = new Editor ();
	return mpriv->editor;
}

static void
gecko_editor_realize (GtkWidget *widget)
{
	GeckoEditorPrivate *mpriv = GECKO_EDITOR (widget)->priv;

	GTK_WIDGET_CLASS (parent_class)->realize (widget);

	/* Initialise our helper class */
	nsresult rv;
	rv = mpriv->editor->Init ("html", TRUE);
	if (NS_FAILED (rv)) {
		g_warning ("`GeckoEditor' initialization failed for %p\n", widget);
		return;
	}
}

static void
gecko_editor_finalize (GObject *object)
{
	GeckoEditorPrivate *mpriv = GECKO_EDITOR (object)->priv;

	/* deleted by root instance */
       	mpriv->editor = nsnull;

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gecko_editor_class_init (GeckoEditorClass *klass)
{
        GObjectClass *object_class = G_OBJECT_CLASS (klass);
     	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass); 

	parent_class = (GObjectClass *) g_type_class_peek_parent (klass);

	object_class->finalize = gecko_editor_finalize;

	widget_class->realize = gecko_editor_realize;

	GECKO_BROWSER_CLASS (parent_class)->create_impl = gecko_editor_create_impl;

	g_type_class_add_private (object_class, sizeof(GeckoEditorPrivate));
}

static void
gecko_editor_init (GeckoEditor *self)
{
        self->priv = GECKO_EDITOR_GET_PRIVATE (self);
}

G_DEFINE_TYPE (GeckoEditor, gecko_editor, GECKO_TYPE_BROWSER)

GeckoEditor *
gecko_editor_new (void)
{
	return GECKO_EDITOR (g_object_new (GECKO_TYPE_EDITOR, NULL));
}
