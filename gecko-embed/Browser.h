/*
 *  Copyright (C) 2000-2004 Marco Pesenti Gritti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#ifndef BROWSER_H
#define BROWSER_H

#include "config.h"

#include "gecko-browser.h"

#include <gtk/gtkwidget.h>

#include <gtkmozembed.h>
#include <nsCOMPtr.h>
#include <nsIWebBrowser.h>
#include <nsIDOMDocument.h>
#include <nsIDOMWindow.h>
#include <nsIWebBrowserFocus.h>
#include <nsIURI.h>

struct GeckoBrowserBase
{
public:
	virtual ~GeckoBrowserBase() {}
};

class Browser : public GeckoBrowserBase
{
public:
	Browser();
	virtual ~Browser();

	nsresult Init (GtkMozEmbed *mozembed);
	nsresult Destroy (void);

	nsresult ScrollTo (PRInt32 aX, PRInt32 aY);
	nsresult GetScrollX (PRInt32 *aX);
	nsresult GetScrollY (PRInt32 *aY);
	nsresult GetDocumentURI (nsIURI **aURI);

	nsresult GetNImages (PRUint32 *num);
	nsresult GetImageAddress (PRUint32 index, nsAString &aAddress);

	nsCOMPtr<nsIWebBrowser> mWebBrowser;

protected:
	nsCOMPtr<nsIDOMWindow> mDOMWindow;

private:
	GtkWidget *mEmbed;

	nsCOMPtr<nsIDOMDocument> mTargetDocument;
	nsCOMPtr<nsIWebBrowserFocus> mWebBrowserFocus;
	PRBool mInitialized;

	nsresult GetImagesCollection (nsIDOMHTMLCollection **images);
};

#endif /* !BROWSER_H */
