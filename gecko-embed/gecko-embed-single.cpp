/*  vim:set ts=8 noet sw=8:
 *  Copyright (C) 2000-2004 Marco Pesenti Gritti
 *  Copyright (C) 2003 Robert Marcano
 *  Copyright (C) 2003, 2004, 2005, 2006 Christian Persch
 *  Copyright (C) 2005 Crispin Flowerday
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#include "config.h"

#include "gecko-embed-single.h"

#include "glib.h"
#include "gtkmozembed.h"
#include "gtkmozembed_internal.h"

#include <nsCOMPtr.h>
#include <nsMemory.h>
#include <nsEmbedString.h>
#include <nsIPrefService.h>
#include <nsIServiceManager.h>
#include <nsILocalFile.h>
#ifdef HAVE_MOZILLA_TOOLKIT
#include <nsServiceManagerUtils.h>
#endif

#define DEFAULT_PROFILE_FILE SHARE_DIR"/default-prefs.js"

static void gecko_embed_single_class_init	(GeckoEmbedSingleClass *klass);
static void gecko_embed_single_init		(GeckoEmbedSingle *ges);

static GObjectClass *parent_class = NULL;

GType
gecko_embed_single_get_type (void)
{
       static GType type = 0;

        if (G_UNLIKELY (type == 0))
        {
                static const GTypeInfo our_info =
                {
                        sizeof (GeckoEmbedSingleClass),
                        NULL, /* base_init */
                        NULL, /* base_finalize */
                        (GClassInitFunc) gecko_embed_single_class_init,
                        NULL, /* class_finalize */
                        NULL, /* class_data */
                        sizeof (GeckoEmbedSingle),
                        0,    /* n_preallocs */
                        (GInstanceInitFunc) gecko_embed_single_init
                };

		type = g_type_register_static (G_TYPE_OBJECT,
					       "GeckoEmbedSingle",
					       &our_info,
					       (GTypeFlags)0);
	}

        return type;
}

static gboolean
gecko_set_default_prefs (GeckoEmbedSingle *mes)
{
	nsCOMPtr<nsIPrefService> prefService;

        prefService = do_GetService (NS_PREFSERVICE_CONTRACTID);
	NS_ENSURE_TRUE (prefService, FALSE);

        /* read our predefined default prefs */
        nsresult rv;
        nsCOMPtr<nsILocalFile> file;
        NS_NewNativeLocalFile(nsEmbedCString(DEFAULT_PROFILE_FILE),
			      PR_TRUE, getter_AddRefs(file));
	if (!file) return FALSE;

        rv = prefService->ReadUserPrefs (file);                                                                              
        if (NS_FAILED(rv))
        {
                g_warning ("failed to read default preferences, error: %x", rv);
		return FALSE;
        }

	return TRUE;
}

GeckoEmbedSingle *
gecko_embed_single_new (void)
{
	return GECKO_EMBED_SINGLE (g_object_new (GECKO_TYPE_EMBED_SINGLE, NULL));
}

gboolean
gecko_embed_single_startup (GeckoEmbedSingle *single)
{
	/* Set gecko binary path */
	gtk_moz_embed_set_comp_path (MOZILLA_HOME);

	/* Fire up the beast */
	gtk_moz_embed_push_startup ();

	if (!gecko_set_default_prefs (single))
	{
		return FALSE;
	}

	return TRUE;
}

void
gecko_embed_single_set_profile_path (GeckoEmbedSingle *single,
				     const char       *path)
{
	gtk_moz_embed_set_profile_path ((char *)path, "browser");
}

static void
gecko_embed_single_init (GeckoEmbedSingle *mes)
{
}

static void
gecko_embed_single_finalize (GObject *object)
{
	/* Destroy EphyEmbedSingle before because some
	 * services depend on xpcom */
	G_OBJECT_CLASS (parent_class)->finalize (object);
	
	gtk_moz_embed_pop_startup ();

#ifdef HAVE_GECKO_1_9
	NS_LogTerm ();
#endif
}

static void
gecko_embed_single_class_init (GeckoEmbedSingleClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = (GObjectClass *) g_type_class_peek_parent (klass);

	object_class->finalize = gecko_embed_single_finalize;
}
