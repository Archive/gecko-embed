/*
 *  Copyright (C) 2006 Rob Staudinger
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "mozilla-config.h"

#include "config.h"

#include "Editor.h"

#include <nsICommandManager.h>
#include <nsIEditingSession.h>
#include "nsIInterfaceRequestorUtils.h"

Editor::Editor ()
{
}

Editor::~Editor ()
{
}

nsresult 
Editor::Init (const char *aEditorType, bool doAfterUriLoad)
{
	nsresult rv;

	/* TODO do someting with edit_session, cmd_mgr */

	nsCOMPtr<nsIEditingSession> edit_session;
	edit_session = do_GetInterface (mWebBrowser, &rv);
	NS_ENSURE_SUCCESS (rv, NS_ERROR_FAILURE);

	nsCOMPtr<nsICommandManager> cmd_mgr; 
	cmd_mgr = do_GetInterface (mWebBrowser, &rv);
	NS_ENSURE_SUCCESS (rv, NS_ERROR_FAILURE);

	/* NOTE: For Moz 1.1 and below, the middle parameter was not used */
	rv = edit_session->MakeWindowEditable (mDOMWindow, aEditorType, doAfterUriLoad);
	return rv;
}
