/*
 *  Copyright (C) 2000-2004 Marco Pesenti Gritti
 *  Copyright (C) 2003, 2004, 2005 Christian Persch
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  $Id$
 */

#include "mozilla-config.h"

#include "config.h"

#include "Browser.h"

#include <gtkmozembed_internal.h>
#include <unistd.h>
#include <nsEmbedString.h>
#include <nsIWebNavigation.h>
#include <nsIDOMHTMLDocument.h>
#include <nsIDOMHTMLCollection.h>
#include <nsIDOMHTMLImageElement.h>
#include "nsIInterfaceRequestorUtils.h"

Browser::Browser ()
{
	mInitialized = PR_FALSE;
}

Browser::~Browser ()
{
}

nsresult Browser::Init (GtkMozEmbed *mozembed)
{
	if (mInitialized) return NS_OK;

	mEmbed = GTK_WIDGET (mozembed);

	gtk_moz_embed_get_nsIWebBrowser (mozembed,
					 getter_AddRefs(mWebBrowser));
	NS_ENSURE_TRUE (mWebBrowser, NS_ERROR_FAILURE);

	mWebBrowserFocus = do_QueryInterface (mWebBrowser);
	NS_ENSURE_TRUE (mWebBrowserFocus, NS_ERROR_FAILURE);

	mWebBrowser->GetContentDOMWindow (getter_AddRefs (mDOMWindow));
	NS_ENSURE_TRUE (mDOMWindow, NS_ERROR_FAILURE);

	/* This will instantiate an about:blank doc if necessary */
	nsresult rv;
	nsCOMPtr<nsIDOMDocument> domDocument;
	rv = mDOMWindow->GetDocument (getter_AddRefs (domDocument));
	NS_ENSURE_SUCCESS (rv, NS_ERROR_FAILURE);

	mInitialized = PR_TRUE;

	return NS_OK;
}

nsresult Browser::Destroy ()
{
      	mWebBrowser = nsnull;
	mDOMWindow = nsnull;
	mEmbed = nsnull;

	mInitialized = PR_FALSE;

	return NS_OK;
}

nsresult
Browser::ScrollTo (PRInt32 aX, PRInt32 aY)
{
	nsCOMPtr<nsIDOMWindow> DOMWindow;

	mWebBrowserFocus->GetFocusedWindow (getter_AddRefs(DOMWindow));
	if (!DOMWindow)
	{
		DOMWindow = mDOMWindow;
	}
	NS_ENSURE_TRUE (DOMWindow, NS_ERROR_FAILURE);

	return DOMWindow->ScrollTo (aX, aY);
}

nsresult
Browser::GetScrollX (PRInt32 *aX)
{
	nsCOMPtr<nsIDOMWindow> DOMWindow;

	mWebBrowserFocus->GetFocusedWindow (getter_AddRefs(DOMWindow));
	if (!DOMWindow)
	{
		DOMWindow = mDOMWindow;
	}
	NS_ENSURE_TRUE (DOMWindow, NS_ERROR_FAILURE);

	return DOMWindow->GetScrollX (aX);
}

nsresult
Browser::GetScrollY (PRInt32 *aY)
{
	nsCOMPtr<nsIDOMWindow> DOMWindow;

	mWebBrowserFocus->GetFocusedWindow (getter_AddRefs(DOMWindow));
	if (!DOMWindow)
	{
		DOMWindow = mDOMWindow;
	}
	NS_ENSURE_TRUE (DOMWindow, NS_ERROR_FAILURE);

	return DOMWindow->GetScrollY (aY);
}

nsresult Browser::GetDocumentURI (nsIURI **aURI)
{
	if (!mDOMWindow) return NS_ERROR_NOT_INITIALIZED;

	nsresult rv;
	nsCOMPtr<nsIWebNavigation> webNav (do_GetInterface (mDOMWindow, &rv));
	NS_ENSURE_SUCCESS (rv, rv);

	return webNav->GetCurrentURI (aURI);
}

nsresult Browser::GetImagesCollection (nsIDOMHTMLCollection **images)
{
	nsresult rv;

	nsCOMPtr<nsIDOMDocument> domDocument;
	rv = mDOMWindow->GetDocument (getter_AddRefs (domDocument));
	NS_ENSURE_SUCCESS (rv, NS_ERROR_FAILURE);

	nsCOMPtr<nsIDOMHTMLDocument> htmlDocument;
	htmlDocument = do_QueryInterface(domDocument, &rv);

	nsCOMPtr<nsIDOMHTMLCollection> collection;
	return htmlDocument->GetImages(images);
}

nsresult Browser::GetNImages (PRUint32 *num)
{
	nsresult rv;

	nsCOMPtr<nsIDOMHTMLCollection> images;
	rv = GetImagesCollection(getter_AddRefs(images));
	NS_ENSURE_SUCCESS (rv, NS_ERROR_FAILURE);
	
	return images->GetLength(num);
}

nsresult Browser::GetImageAddress (PRUint32 index, nsAString &aAddress)
{
	nsresult rv;

	nsCOMPtr<nsIDOMHTMLCollection> images;
	rv = GetImagesCollection(getter_AddRefs(images));
	NS_ENSURE_SUCCESS (rv, NS_ERROR_FAILURE);

	nsCOMPtr<nsIDOMNode> node;
	rv = images->Item(index, getter_AddRefs(node));

	nsCOMPtr<nsIDOMHTMLImageElement> img;
	img = do_QueryInterface(node, &rv);	
	NS_ENSURE_SUCCESS (rv, NS_ERROR_FAILURE);

	return img->GetSrc (aAddress);
}
