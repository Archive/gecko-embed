#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* include this first, before NO_IMPORT_PYGOBJECT is defined */
#include <pygobject.h>

void pygeckoembed_register_classes (PyObject *d);

extern PyMethodDef pygeckoembed_functions[];

DL_EXPORT(void)
initgeckoembed(void)
{
    PyObject *m, *d;

    init_pygobject ();

    m = Py_InitModule ("geckoembed", pygeckoembed_functions);
    d = PyModule_GetDict (m);

    pygeckoembed_register_classes (d);

    if (PyErr_Occurred ()) {
        Py_FatalError ("can't initialise module geckoembed");
    }
}
