/* -- THIS FILE IS GENERATED - DO NOT EDIT *//* -*- Mode: C; c-basic-offset: 4 -*- */

#include <Python.h>



#line 4 "geckoembed.override"
#include <Python.h>

#include "pygobject.h"
#include <gecko-embed/gecko-browser.h>
#include <gecko-embed/gecko-editor.h>

static PyObject *
_helper_wrap_string_glist (GList *list)
{
    GList *tmp;
    PyObject *py_list;

    if ((py_list = PyList_New(0)) == NULL) {
        g_list_foreach(list, (GFunc)g_free, NULL);
        g_list_free(list);
        return NULL;
    }
    for (tmp = list; tmp != NULL; tmp = tmp->next) {
        PyObject *str_obj =  PyString_FromString ((char*)tmp->data);

        if (str_obj == NULL) {
            g_list_foreach(list, (GFunc)g_free, NULL);
            g_list_free(list);
            Py_DECREF(py_list);
            return NULL;
        }
        PyList_Append(py_list, str_obj);
        Py_DECREF(str_obj);
    }
    g_list_foreach(list, (GFunc)g_free, NULL);
    g_list_free(list);
    return py_list;
}

#line 43 "geckoembed.c"


/* ---------- types from other modules ---------- */
static PyTypeObject *_PyGObject_Type;
#define PyGObject_Type (*_PyGObject_Type)
static PyTypeObject *_PyGtkObject_Type;
#define PyGtkObject_Type (*_PyGtkObject_Type)
static PyTypeObject *_PyGtkBin_Type;
#define PyGtkBin_Type (*_PyGtkBin_Type)


/* ---------- forward type declarations ---------- */
PyTypeObject G_GNUC_INTERNAL PyGeckoBrowser_Type;
PyTypeObject G_GNUC_INTERNAL PyGeckoEditor_Type;

#line 59 "geckoembed.c"



/* ----------- GeckoBrowser ----------- */

static int
_wrap_gecko_browser_new(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char* kwlist[] = { NULL };

    if (!PyArg_ParseTupleAndKeywords(args, kwargs,
                                     ":geckoembed.Browser.__init__",
                                     kwlist))
        return -1;

    pygobject_constructv(self, 0, NULL);
    if (!self->obj) {
        PyErr_SetString(
            PyExc_RuntimeError, 
            "could not create geckoembed.Browser object");
        return -1;
    }
    return 0;
}

static PyObject *
_wrap_gecko_browser_load_address(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "address", NULL };
    char *address;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs,"s:GeckoBrowser.load_address", kwlist, &address))
        return NULL;
    
    gecko_browser_load_address(GECKO_BROWSER(self->obj), address);
    
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_gecko_browser_can_go_back(PyGObject *self)
{
    int ret;

    
    ret = gecko_browser_can_go_back(GECKO_BROWSER(self->obj));
    
    return PyBool_FromLong(ret);

}

static PyObject *
_wrap_gecko_browser_can_go_forward(PyGObject *self)
{
    int ret;

    
    ret = gecko_browser_can_go_forward(GECKO_BROWSER(self->obj));
    
    return PyBool_FromLong(ret);

}

static PyObject *
_wrap_gecko_browser_go_back(PyGObject *self)
{
    
    gecko_browser_go_back(GECKO_BROWSER(self->obj));
    
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_gecko_browser_go_forward(PyGObject *self)
{
    
    gecko_browser_go_forward(GECKO_BROWSER(self->obj));
    
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_gecko_browser_reload(PyGObject *self)
{
    
    gecko_browser_reload(GECKO_BROWSER(self->obj));
    
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_gecko_browser_get_title(PyGObject *self)
{
    gchar *ret;

    
    ret = gecko_browser_get_title(GECKO_BROWSER(self->obj));
    
    if (ret) {
        PyObject *py_ret = PyString_FromString(ret);
        g_free(ret);
        return py_ret;
    }
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
_wrap_gecko_browser_get_address(PyGObject *self)
{
    gchar *ret;

    
    ret = gecko_browser_get_address(GECKO_BROWSER(self->obj));
    
    if (ret) {
        PyObject *py_ret = PyString_FromString(ret);
        g_free(ret);
        return py_ret;
    }
    Py_INCREF(Py_None);
    return Py_None;
}

static const PyMethodDef _PyGeckoBrowser_methods[] = {
    { "load_address", (PyCFunction)_wrap_gecko_browser_load_address, METH_VARARGS|METH_KEYWORDS,
      NULL },
    { "can_go_back", (PyCFunction)_wrap_gecko_browser_can_go_back, METH_NOARGS,
      NULL },
    { "can_go_forward", (PyCFunction)_wrap_gecko_browser_can_go_forward, METH_NOARGS,
      NULL },
    { "go_back", (PyCFunction)_wrap_gecko_browser_go_back, METH_NOARGS,
      NULL },
    { "go_forward", (PyCFunction)_wrap_gecko_browser_go_forward, METH_NOARGS,
      NULL },
    { "reload", (PyCFunction)_wrap_gecko_browser_reload, METH_NOARGS,
      NULL },
    { "get_title", (PyCFunction)_wrap_gecko_browser_get_title, METH_NOARGS,
      NULL },
    { "get_address", (PyCFunction)_wrap_gecko_browser_get_address, METH_NOARGS,
      NULL },
    { NULL, NULL, 0, NULL }
};

PyTypeObject G_GNUC_INTERNAL PyGeckoBrowser_Type = {
    PyObject_HEAD_INIT(NULL)
    0,                                 /* ob_size */
    "geckoembed.Browser",                   /* tp_name */
    sizeof(PyGObject),          /* tp_basicsize */
    0,                                 /* tp_itemsize */
    /* methods */
    (destructor)0,        /* tp_dealloc */
    (printfunc)0,                      /* tp_print */
    (getattrfunc)0,       /* tp_getattr */
    (setattrfunc)0,       /* tp_setattr */
    (cmpfunc)0,           /* tp_compare */
    (reprfunc)0,             /* tp_repr */
    (PyNumberMethods*)0,     /* tp_as_number */
    (PySequenceMethods*)0, /* tp_as_sequence */
    (PyMappingMethods*)0,   /* tp_as_mapping */
    (hashfunc)0,             /* tp_hash */
    (ternaryfunc)0,          /* tp_call */
    (reprfunc)0,              /* tp_str */
    (getattrofunc)0,     /* tp_getattro */
    (setattrofunc)0,     /* tp_setattro */
    (PyBufferProcs*)0,  /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,                      /* tp_flags */
    NULL,                        /* Documentation string */
    (traverseproc)0,     /* tp_traverse */
    (inquiry)0,             /* tp_clear */
    (richcmpfunc)0,   /* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,          /* tp_iter */
    (iternextfunc)0,     /* tp_iternext */
    (struct PyMethodDef*)_PyGeckoBrowser_methods, /* tp_methods */
    (struct PyMemberDef*)0,              /* tp_members */
    (struct PyGetSetDef*)0,  /* tp_getset */
    NULL,                              /* tp_base */
    NULL,                              /* tp_dict */
    (descrgetfunc)0,    /* tp_descr_get */
    (descrsetfunc)0,    /* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)_wrap_gecko_browser_new,             /* tp_init */
    (allocfunc)0,           /* tp_alloc */
    (newfunc)0,               /* tp_new */
    (freefunc)0,             /* tp_free */
    (inquiry)0              /* tp_is_gc */
};



/* ----------- GeckoEditor ----------- */

static int
_wrap_gecko_edit_new(PyGObject *self, PyObject *args, PyObject *kwargs)
{
    static char* kwlist[] = { NULL };

    if (!PyArg_ParseTupleAndKeywords(args, kwargs,
                                     ":geckoembed.Editor.__init__",
                                     kwlist))
        return -1;

    pygobject_constructv(self, 0, NULL);
    if (!self->obj) {
        PyErr_SetString(
            PyExc_RuntimeError, 
            "could not create geckoembed.Editor object");
        return -1;
    }
    return 0;
}

PyTypeObject G_GNUC_INTERNAL PyGeckoEditor_Type = {
    PyObject_HEAD_INIT(NULL)
    0,                                 /* ob_size */
    "geckoembed.Editor",                   /* tp_name */
    sizeof(PyGObject),          /* tp_basicsize */
    0,                                 /* tp_itemsize */
    /* methods */
    (destructor)0,        /* tp_dealloc */
    (printfunc)0,                      /* tp_print */
    (getattrfunc)0,       /* tp_getattr */
    (setattrfunc)0,       /* tp_setattr */
    (cmpfunc)0,           /* tp_compare */
    (reprfunc)0,             /* tp_repr */
    (PyNumberMethods*)0,     /* tp_as_number */
    (PySequenceMethods*)0, /* tp_as_sequence */
    (PyMappingMethods*)0,   /* tp_as_mapping */
    (hashfunc)0,             /* tp_hash */
    (ternaryfunc)0,          /* tp_call */
    (reprfunc)0,              /* tp_str */
    (getattrofunc)0,     /* tp_getattro */
    (setattrofunc)0,     /* tp_setattro */
    (PyBufferProcs*)0,  /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,                      /* tp_flags */
    NULL,                        /* Documentation string */
    (traverseproc)0,     /* tp_traverse */
    (inquiry)0,             /* tp_clear */
    (richcmpfunc)0,   /* tp_richcompare */
    offsetof(PyGObject, weakreflist),             /* tp_weaklistoffset */
    (getiterfunc)0,          /* tp_iter */
    (iternextfunc)0,     /* tp_iternext */
    (struct PyMethodDef*)NULL, /* tp_methods */
    (struct PyMemberDef*)0,              /* tp_members */
    (struct PyGetSetDef*)0,  /* tp_getset */
    NULL,                              /* tp_base */
    NULL,                              /* tp_dict */
    (descrgetfunc)0,    /* tp_descr_get */
    (descrsetfunc)0,    /* tp_descr_set */
    offsetof(PyGObject, inst_dict),                 /* tp_dictoffset */
    (initproc)_wrap_gecko_edit_new,             /* tp_init */
    (allocfunc)0,           /* tp_alloc */
    (newfunc)0,               /* tp_new */
    (freefunc)0,             /* tp_free */
    (inquiry)0              /* tp_is_gc */
};



/* ----------- functions ----------- */

static PyObject *
_wrap_gecko_browser_set_profile_path(PyObject *self, PyObject *args, PyObject *kwargs)
{
    static char *kwlist[] = { "path", NULL };
    char *path;

    if (!PyArg_ParseTupleAndKeywords(args, kwargs,"s:set_profile_path", kwlist, &path))
        return NULL;
    
    gecko_browser_set_profile_path(path);
    
    Py_INCREF(Py_None);
    return Py_None;
}

const PyMethodDef pygeckoembed_functions[] = {
    { "set_profile_path", (PyCFunction)_wrap_gecko_browser_set_profile_path, METH_VARARGS|METH_KEYWORDS,
      NULL },
    { NULL, NULL, 0, NULL }
};

/* initialise stuff extension classes */
void
pygeckoembed_register_classes(PyObject *d)
{
    PyObject *module;

    if ((module = PyImport_ImportModule("gobject")) != NULL) {
        _PyGObject_Type = (PyTypeObject *)PyObject_GetAttrString(module, "GObject");
        if (_PyGObject_Type == NULL) {
            PyErr_SetString(PyExc_ImportError,
                "cannot import name GObject from gobject");
            return ;
        }
    } else {
        PyErr_SetString(PyExc_ImportError,
            "could not import gobject");
        return ;
    }
    if ((module = PyImport_ImportModule("gtk")) != NULL) {
        _PyGtkObject_Type = (PyTypeObject *)PyObject_GetAttrString(module, "Object");
        if (_PyGtkObject_Type == NULL) {
            PyErr_SetString(PyExc_ImportError,
                "cannot import name Object from gtk");
            return ;
        }
        _PyGtkBin_Type = (PyTypeObject *)PyObject_GetAttrString(module, "Bin");
        if (_PyGtkBin_Type == NULL) {
            PyErr_SetString(PyExc_ImportError,
                "cannot import name Bin from gtk");
            return ;
        }
    } else {
        PyErr_SetString(PyExc_ImportError,
            "could not import gtk");
        return ;
    }


#line 385 "geckoembed.c"
    pygobject_register_class(d, "GeckoBrowser", GECKO_TYPE_BROWSER, &PyGeckoBrowser_Type, Py_BuildValue("(O)", &PyGtkBin_Type));
    pyg_set_object_has_new_constructor(GECKO_TYPE_BROWSER);
    pygobject_register_class(d, "GeckoEditor", GECKO_TYPE_EDITOR, &PyGeckoEditor_Type, Py_BuildValue("(O)", &PyGeckoBrowser_Type));
    pyg_set_object_has_new_constructor(GECKO_TYPE_EDITOR);
}
