#!/usr/bin/env python

import pygtk
pygtk.require('2.0')
import gtk
import geckoembed

class Base:
	def __init__(self):
		window = gtk.Window(gtk.WINDOW_TOPLEVEL)

		hbox = gtk.HBox()

		geckoembed.set_profile_path('/home/marco/.sugar')

		self.embed = geckoembed.Browser()
		self.embed.connect("open_address", self.open_address)
		hbox.add(self.embed)
		self.embed.show()

		button = gtk.Button("Images")
		button.connect("clicked", self.images_click_cb)
		hbox.add(button)
		button.show()

		window.add(hbox)
		hbox.show()

		window.show()

		self.embed.load_address("http://www.gmail.com")

	def images_click_cb(self, button):
		print self.embed.list_images()

	def open_address(self, embed, uri):
		print uri
		return False

	def main(self):
		gtk.main()

base = Base()
base.main()
